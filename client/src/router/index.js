import { createRouter, createWebHashHistory } from 'vue-router';
import store from '@/store/index';
import auth from '@/store/modules/auth';
import Home from '../views/Home.vue';

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    meta: { title: 'Odisee trajectplanner' },
  },
  {
    path: '/nieuwtraject',
    name: 'Dashboard',
    component: () => import('../views/Dashboard.vue'),
    meta: { title: 'Traject aanmaken' },
  },
  {
    path: '/trajecten',
    name: 'Trajectories',
    component: () => import('../views/TrajectoriesPage.vue'),
    children: [
      {
        path: 'favoriet',
        name: 'Favorite',
        component: () => import('../components/UI/organisms/TrajectoriesFav.vue'),
        meta: { title: 'Favoriete trajecten' },
      },
      {
        path: 'gedeeld',
        name: 'Shared',
        component: () => import('../components/UI/organisms/TrajectoriesShared.vue'),
        meta: { title: 'Gedeelde trajecten' },
      },
      {
        path: 'geaccepteerd',
        name: 'Accepted',
        component: () => import('../components/UI/organisms/TrajectoriesAccepted.vue'),
        meta: { title: 'Geaccepteerde trajecten' },
      },
    ],
    meta: { requiresAuth: true, title: 'Trajecten' },
  },
  {
    path: '/trajecten/:id',
    name: 'TrajectoryContent',
    component: () => import('../views/TrajectoryContentPage.vue'),
    meta: { requiresAuth: true, title: 'Inhoud van trajact' },
  },
  {
    path: '/inloggen',
    name: 'Login',
    component: () => import('../views/Login.vue'),
  },
  {
    path: '/registreren',
    name: 'Register',
    component: () => import('../views/Register.vue'),
    // meta: { requiresUnAuth: true },
  },
  {
    path: '/:test(.*)',
    name: 'Notfound',
    component: () => import('../views/NotFound.vue'),
  },
];

const router = createRouter({
  history: createWebHashHistory(process.env.BASE_URL),
  routes,
});

router.beforeEach((to, from, next) => {
  if (to.matched.some((record) => record.meta.requiresAuth)) {
    if (!auth.state.user) {
      store.dispatch('auth/tryLogIn')
        .then(() => next())
        .catch(() => next({ name: 'Login' }));
    } else {
      next();
    }
  } else {
    next();
  }
});

router.afterEach((to) => {
  if (to.meta.title) {
    document.title = to.meta.title || 'test';
  }
});

export default router;
