export default {
  subjects(state) {
    return state.subjects;
  },
  chosenSubjects(state) {
    return state.chosenSubjects;
  },
};
