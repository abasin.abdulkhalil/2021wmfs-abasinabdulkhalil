import myAxios from '@/customaxios/myAxios';

export default {
  async getSubjects(context) {
    try {
      await myAxios.get('api/subjects').then((response) => {
        const subjects = response.data.data;
        context.commit('setSubjects', {
          subjects,
        });
      });
    } catch (error) {
      /* eslint-disable */
      console.log(error);
    }
  },
};
