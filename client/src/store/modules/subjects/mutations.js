export default {
  setSubjects(state, payload) {
    state.subjects = payload.subjects;
  },
  addSubject(state, subject) {
    state.chosenSubjects.push(subject);
  },
};
