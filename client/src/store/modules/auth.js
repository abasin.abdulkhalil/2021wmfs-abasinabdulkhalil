import myAxios from '@/customaxios/myAxios';

export default {
  namespaced: true,
  state: {
    user: null,
  },
  getters: {
    user: (state) => state.user,
  },
  mutations: {
    setUser(state, data) {
      state.user = data;
    },
  },
  actions: {
    async tryLogIn({ commit }) {
      if (document.cookie.indexOf('XSRF-TOKEN') === -1) {
        return;
      }
      await myAxios.get('api/user')
        .then((response) => {
          commit('setUser', response.data);
        });
    },
    async logIn({ commit }, formData) {
      await myAxios.get('sanctum/csrf-cookie');
      await myAxios.post('sanctum/login', formData)
        .then((response) => {
          commit('setUser', response.data);
        });
    },
    async logOut({ commit }) {
      await myAxios.post('/sanctum/logout');
      document.cookie = 'XSRF-TOKEN=;path=/;expires=Thu, 01 Jan 1970 00:00:00 UTC; Secure;';
      commit('setUser', null);
    },

    async register({ commit }, formData) {
      await myAxios.get('sanctum/csrf-cookie');
      await myAxios.post('api/register', formData);
      await myAxios.post('sanctum/login', formData)
        .then((response) => {
          commit('setUser', response.data);
          // console.log(response.data);
        });
    },
  },
};
