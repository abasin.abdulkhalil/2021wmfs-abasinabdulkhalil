export default {
  setTrajectories(state, payload) {
    state.trajectories = payload.trajectories;
  },
};
