import myAxios from '@/customaxios/myAxios';

export default {
  async getTrajectories(context) {
    try {
      myAxios.get('api/trajectories').then((response) => {
        const trajectories = response.data.data;
        context.commit('setTrajectories', {
          trajectories,
        });
      });
    } catch (error) {
      /* eslint-disable */
      console.log(error);
    }
  },

  async deleteTrajectory(context, id) {
    await myAxios.delete(`api/trajectories/${id}`);
  },
  async shareTrajectory(context, id) {
    await myAxios.post(`api/trajectories/${id}/share`);
  },
  async favTrajectory(context, id) {
    await myAxios.post(`api/trajectories/${id}/favorite`);
  },

  async addTrajectory(context, data) {
    await myAxios.post('api/trajectories', { subjects: data });
  },
};
