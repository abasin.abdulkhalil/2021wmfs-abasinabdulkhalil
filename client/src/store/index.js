import { createStore } from 'vuex';
import auth from './modules/auth';
import subjects from './modules/subjects/index';
import trajectories from './modules/trajectories/index';

export default createStore({
  modules: {
    auth,
    subjects,
    trajectories,
  },
  namespaced: true,
  state: {
  },
  getters: {
    navItems: (state) => state.navItems,
  },

  mutations: {
  },
  actions: {
  },
});
