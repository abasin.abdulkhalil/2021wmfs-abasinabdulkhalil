<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class trajectPlanner extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // seed roles table
        DB::table('roles')->insert([
            [
                'role' => 'admin',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'role' => 'student',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ]
        ]);

        //seed users table
        DB::table('users')->insert([
            [
                'name' => 'Abasin',
                'surname' => 'Abdul Khalil',
                'email' => 'abasin.abdulkhalil@yahoo.com',
                'password' => Hash::make('Azerty123'),
                'role_id' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'name' => 'Atal',
                'surname' => 'Abdul Khalil',
                'email' => 'atal.abdulkhalil@yahoo.com',
                'password' => Hash::make('Azerty123'),
                'role_id' => 2,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'name' => 'Lewis',
                'surname' => 'De Snoek',
                'email' => 'lewis.desnoek@yahoo.com',
                'password' => Hash::make('Azerty123'),
                'role_id' => 2,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'name' => 'Joren',
                'surname' => 'Goessens',
                'email' => 'joren.goessens@yahoo.com',
                'password' => Hash::make('Azerty123'),
                'role_id' => 2,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ]
        ]);

        // seed trajectories table
        DB::table('trajectories')->insert([
            [
                'favorite' => 0,
                'accepted' => 0,
                'shared' => 1,
                'user_id' => 2,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'favorite' => 0,
                'accepted' => 0,
                'shared' => 0,
                'user_id' => 2,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'favorite' => 0,
                'accepted' => 0,
                'shared' => 1,
                'user_id' => 3,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'favorite' => 0,
                'accepted' => 0,
                'shared' => 1,
                'user_id' => 4,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ]
        ]);

        // seed subjects table
        DB::table('subjects')->insert([
            [
                'name' => 'Professionele Communicatie',
                'code' => 'PC01-15',
                'credit' => 10,
                'period' => 'both',
                'semester' => 'first',
                'phase' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'name' => 'Ethiek',
                'code' => 'ET01-15',
                'credit' => 20,
                'period' => 'both',
                'semester' => 'first',
                'phase' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'name' => 'C# OO Programming',
                'code' => 'CP01-07',
                'credit' => 20,
                'period' => 'first',
                'semester' => 'first',
                'phase' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'name' => 'C# Programming Techniques',
                'code' => 'CT09-15',
                'credit' => 10,
                'period' => 'second',
                'semester' => 'second',
                'phase' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'name' => 'Web & Mobile SS',
                'code' => 'WS01-15',
                'credit' => 20,
                'period' => 'both',
                'semester' => 'second',
                'phase' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'name' => 'Project & wetenschappelijk Rapporteren',
                'code' => 'PR01-15',
                'credit' => 20,
                'period' => 'both',
                'semester' => 'second',
                'phase' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'name' => 'Algorithms & Data',
                'code' => 'AD09-15',
                'credit' => 10,
                'period' => 'first',
                'semester' => 'first',
                'phase' => 2,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'name' => 'Data Science',
                'code' => 'DS09-15',
                'credit' => 20,
                'period' => 'second',
                'semester' => 'first',
                'phase' => 2,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'name' => 'Web & Mobile FS',
                'code' => 'WF01-15',
                'credit' => 20,
                'period' => 'both',
                'semester' => 'first',
                'phase' => 2,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'name' => 'Python',
                'code' => 'PYTH-15',
                'credit' => 10,
                'period' => 'second',
                'semester' => 'second',
                'phase' => 2,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'name' => 'Web & Mobile Next Level',
                'code' => 'WNO1-15',
                'credit' => 20,
                'period' => 'both',
                'semester' => 'second',
                'phase' => 2,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'name' => 'Web & Mobile Next Level',
                'code' => 'WNO1-15',
                'credit' => 20,
                'period' => 'both',
                'semester' => 'second',
                'phase' => 2,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'name' => 'C# OO Programming 3',
                'code' => 'C#O1-15',
                'credit' => 10,
                'period' => 'second',
                'semester' => 'first',
                'phase' => 3,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'name' => 'Applied Programming',
                'code' => 'APO1-15',
                'credit' => 20,
                'period' => 'both',
                'semester' => 'first',
                'phase' => 3,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'name' => 'Advanced Programming',
                'code' => 'APD1-15',
                'credit' => 20,
                'period' => 'both',
                'semester' => 'first',
                'phase' => 3,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'name' => 'Scrum',
                'code' => 'SMO1-15',
                'credit' => 10,
                'period' => 'first',
                'semester' => 'second',
                'phase' => 3,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'name' => 'Stage',
                'code' => 'STO1-15',
                'credit' => 20,
                'period' => 'both',
                'semester' => 'second',
                'phase' => 3,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'name' => 'Data Security',
                'code' => 'DSO1-15',
                'credit' => 20,
                'period' => 'first',
                'semester' => 'second',
                'phase' => 3,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ]
        ]);

        // seed subject_trajectory table
        DB::table('subject_trajectory')->insert([
            [
                'subject_id' => 1,
                'trajectory_id' => 1
            ],
            [
                'subject_id' => 2,
                'trajectory_id' => 1
            ],
            [
                'subject_id' => 3,
                'trajectory_id' => 1
            ],
            [
                'subject_id' => 4,
                'trajectory_id' => 1
            ],
            [
                'subject_id' => 5,
                'trajectory_id' => 1
            ],
            [
                'subject_id' => 2,
                'trajectory_id' => 2
            ],
            [
                'subject_id' => 9,
                'trajectory_id' => 2
            ],
            [
                'subject_id' => 8,
                'trajectory_id' => 2
            ],
            [
                'subject_id' => 7,
                'trajectory_id' => 2
            ],
            [
                'subject_id' => 5,
                'trajectory_id' => 2
            ],
            [
                'subject_id' => 6,
                'trajectory_id' => 2
            ],
            [
                'subject_id' => 1,
                'trajectory_id' => 3
            ],
            [
                'subject_id' => 2,
                'trajectory_id' => 3
            ],
            [
                'subject_id' => 3,
                'trajectory_id' => 3
            ],
            [
                'subject_id' => 4,
                'trajectory_id' => 3
            ],
            [
                'subject_id' => 5,
                'trajectory_id' => 3
            ],
            [
                'subject_id' => 2,
                'trajectory_id' => 4
            ],
            [
                'subject_id' => 9,
                'trajectory_id' => 4
            ],
            [
                'subject_id' => 8,
                'trajectory_id' => 4
            ],
            [
                'subject_id' => 7,
                'trajectory_id' => 4
            ],
            [
                'subject_id' => 5,
                'trajectory_id' => 4
            ],
            [
                'subject_id' => 6,
                'trajectory_id' => 4
            ]
        ]);
    }
}
