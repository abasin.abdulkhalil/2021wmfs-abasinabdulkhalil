<?php

namespace App\Http\Controllers;

use App\Models\Subject;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

/**
 * @group Subjects management
 *
 *
 */
class SubjectApiController extends Controller
{
    /**
     * Get a list of all subjects.
     *
     * @response {"data": [
     * {
     * "id": 1,
     * "name": "Professionele Communicatie",
     * "code": "PC01-15",
     * "credit": 3,
     * "period": "both",
     * "semester": "first",
     * "phase": 2,
     * "created_at": "29-03-2021 15:03:36",
     * "updated_at": "29-03-2021 15:03:36"
     * },
     * {
     * "id": 2,
     * "name": "Ethiek",
     * "code": "ET01-15",
     * "credit": 3,
     * "period": "both",
     * "semester": "first",
     * "phase": 2,
     * "created_at": "29-03-2021 15:03:36",
     * "updated_at": "29-03-2021 15:03:36"
     * }
     * ]
     * }
     *
     */
    public function index()
    {
        return ['data' => Subject::all()];
    }

    /**
     * Create a new subject
     *
     * Add a new subject to Database.
     *
     * @bodyParam name string required
     * The name of the subject. Example: Programming
     *
     * @bodyParam code string required
     * The code of the subject. Example: PG01-07
     *
     * @bodyParam credit number required
     * The credit of the subject. Example: 6
     *
     * @bodyParam period enum(first,second,both) required
     * The period of the subject. Example: first
     *
     * @bodyParam semester enum(first,second,both) required
     * The semester of the subject. Example: second
     *
     * @bodyParam phase number required
     * The phase of the subject. Example: 2
     *
     * @response {
     *      "status": 201,
     *      "success": true,
     *      "message": "The subject has been updated"
     * }
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Gate::authorize('isAdmin');
        $request->validate([
            'name' => 'required',
            'code' => 'required | unique:Subjects',
            'credit' => 'required|numeric|min:1',
            'period' => ['required', Rule::in(['first', 'second', 'both'])],
            'semester' => ['required', Rule::in(['first', 'second'])],
            'phase' => ['required', Rule::in([1, 2, 3])]
        ]);
        Subject::create($request->all());
        return response()->json(['message' => 'The subject has been created'], 201);
    }

    /**
     * Get one subjects.
     *
     *
     *
     * @pathParam id number required
     * The id of the subject. Example: 1
     *
     * @response {"data": [
     * {
     * "id": 1,
     * "name": "Professionele Communicatie",
     * "code": "PC01-15",
     * "credit": 3,
     * "period": "both",
     * "semester": "first",
     * "phase": 2,
     * "created_at": "29-03-2021 15:03:36",
     * "updated_at": "29-03-2021 15:03:36"
     * }
     * ]
     * }
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return ['data' => Subject::findorfail($id)];
    }

    /**
     * Update a subject in database.
     *
     * Update a subject in database.
     *
     * @bodyParam name string required
     * The name of the subject. Example: Programming
     *
     *
     * @bodyParam credit number required
     * The credit of the subject. Example: 6
     *
     * @bodyParam period enum(first,second,both) required
     * The period of the subject. Example: first
     *
     * @bodyParam semester enum(first,second,both) required
     * The semester of the subject. Example: second
     *
     * @bodyParam phase number required
     * The phase of the subject. Example: 2
     *
     * @response {
     *      "status": 200,
     *      "success": true,
     *      "message": "The subject has been updated"
     * }
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Gate::authorize('isAdmin');
        $request->validate([
            'name' => 'required',
            //'code' => 'required | unique:Subjects',
            'credit' => 'required|numeric|min:1',
            'period' => ['required', Rule::in(['first', 'second', 'both'])],
            'semester' => ['required', Rule::in(['first', 'second'])],
            'phase' => ['required', Rule::in([1, 2, 3])]
        ]);
        $subject = Subject::findorfail($id);
        $subject->update($request->all());
        return response()->json(['message' => 'The subject has been updated'], 200);
    }

    /**
     * Remove a subject from database.
     *
     * @pathParam id number required
     * The id of the subject. Example: 1
     *
     * @response {
     *      "status": 204,
     *      "success": true,
     *      "message": null
     * }
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Gate::authorize('isAdmin');
        $subject = Subject::findorfail($id);
        $subject->delete();
        return response()->json(null, 204);
    }
}
