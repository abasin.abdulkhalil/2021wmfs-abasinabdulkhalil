<?php

namespace App\Http\Controllers;

use App\Models\Subject;

use App\Models\Trajectory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class SubjectController extends Controller
{
    //
    public function overview(Request $request){
        if ($request->name && $request->semester && $request->phase){
            $subjects = Subject::where('name', 'like', '%' . $request->name . '%')
                ->where('semester', $request->semester)
                ->where('phase', $request->phase)
                ->paginate(10);
        }
        elseif ($request->semester && $request->phase){
            $subjects = Subject::where('phase', $request->phase)
                ->where('semester', $request->semester)
                ->paginate(10);
        }
        elseif ($request->name && $request->semester){
            $subjects = Subject::where('name', 'like', '%' . $request->name . '%')
                ->where('semester', $request->semester)
                ->paginate(10);
        }
        elseif ($request->name && $request->phase){
            $subjects = Subject::where('name', 'like', '%' . $request->name . '%')
                ->where('phase', $request->phase)
                ->paginate(10);
        }
        elseif ($request->name){
            $subjects = Subject::where('name', 'like', '%' . $request->name . '%')->paginate(10);
        }
        elseif ($request->phase){
            $subjects = Subject::where('phase', $request->phase)->paginate(10);
        }
        elseif ($request->semester){
            $subjects = Subject::where('semester', $request->semester)->paginate(10);
        }
        else{
            $subjects = Subject::paginate(10);
        }
//   dd($subjects);
        return view('subjects', ['subjects' => $subjects]);
    }

    public function subjectInfo(Subject $subject){
        return view('subjects-info', ['subject' => $subject]);
    }

    public function subjectShowAdd(){
        //Gate::authorize('isAdmin');
        return view('subjects-add');
    }

    public function subjectAdd(Request $request){
        //Gate::authorize('isAdmin');
        $request->validate([
            'name' => 'required',
            'code' => 'required | unique:Subjects',
            'credit' => 'required|numeric|min:1',
            'period' => ['required', Rule::in(['first', 'second', 'both'])],
            'semester' => ['required', Rule::in(['first', 'second'])],
            'phase' => ['required', Rule::in([1, 2, 3])]
        ]);

        Subject::create($request->all());

        return redirect('/dashboard/subjects');
    }

    public function subjectShowEdit(Subject $subject){
        $subject1 = Subject::findOrFail($subject->id);
        return view('subjects-edit', ['subject' => $subject1]);
    }

    public function subjectEdit(Request $request){
        $request->validate([
            'name' => 'required',
            'credit' => 'required|numeric|min:1',
            'period' => ['required', Rule::in(['first', 'second', 'both'])],
            'semester' => ['required', Rule::in(['first', 'second'])],
            'phase' => ['required', Rule::in([1, 2, 3])]
        ]);

        //dd($request->name);
        $subject = Subject::findOrFail($request->id);

        $subject->name = $request->name;
        $subject->credit = $request->credit;
        $subject->period = $request->period;
        $subject->semester = $request->semester;
        $subject->phase = $request->phase;

        $subject->save();
        return redirect('/dashboard/subjects');
    }

    public function subjectShowDelete(Subject $subject){
        //Gate::authorize('isAdmin');
        return view('delete-model', ['model' => $subject]);

    }

    public function subjectDelete(Subject $subject){
        //Gate::authorize('isAdmin');
        $subject->delete();
        return redirect('/dashboard/subjects');
    }
}
