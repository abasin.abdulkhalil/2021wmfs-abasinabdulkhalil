<?php

namespace App\Http\Controllers;

use App\Models\Trajectory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

/**
 * @group Trajectory management
 *
 *
 */
class TrajectoryApiController extends Controller
{
    /**
     * Get a list of all trajectories.
     *
     * Get a list of all trajectories with a collection of subjects.
     *
     * @response {"data": [
     * {
     * "id": 1,
     * "favorite": 0,
     * "accepted": 0,
     * "shared": 1,
     * "user_id": 2,
     * "created_at": "29-03-2021 15:03:36",
     * "updated_at": "29-03-2021 15:03:36",
     * "subjects": [
     * {
     * "id": 1,
     * "name": "Professionele Communicatie",
     * "code": "PC01-15",
     * "credit": 3,
     * "period": "both",
     * "semester": "first",
     * "phase": 2,
     * "created_at": "29-03-2021 15:03:36",
     * "updated_at": "29-03-2021 15:03:36"
     * },
     * {
     * "id": 2,
     * "name": "Ethiek",
     * "code": "ET01-15",
     * "credit": 3,
     * "period": "both",
     * "semester": "first",
     * "phase": 2,
     * "created_at": "29-03-2021 15:03:36",
     * "updated_at": "29-03-2021 15:03:36"
     * },
     * {
     * "id": 3,
     * "name": "C# OO Programming",
     * "code": "CP01-07",
     * "credit": 6,
     * "period": "first",
     * "semester": "first",
     * "phase": 2,
     * "created_at": "29-03-2021 15:03:36",
     * "updated_at": "29-03-2021 15:03:36"
     * },
     * {
     * "id": 4,
     * "name": "C# Programming Techniques",
     * "code": "CT09-15",
     * "credit": 6,
     * "period": "second",
     * "semester": "first",
     * "phase": 2,
     * "created_at": "29-03-2021 15:03:36",
     * "updated_at": "29-03-2021 15:03:36"
     * },
     * {
     * "id": 5,
     * "name": "Web & Mobile SS",
     * "code": "WS01-15",
     * "credit": 12,
     * "period": "both",
     * "semester": "first",
     * "phase": 2,
     * "created_at": "29-03-2021 15:03:36",
     * "updated_at": "29-03-2021 15:03:36"
     * }
     * ]
     * },
     * {
     * "id": 2,
     * "favorite": 0,
     * "accepted": 0,
     * "shared": 0,
     * "user_id": 2,
     * "created_at": "29-03-2021 15:03:36",
     * "updated_at": "29-03-2021 15:03:36",
     * "subjects": [
     * {
     * "id": 2,
     * "name": "Ethiek",
     * "code": "ET01-15",
     * "credit": 3,
     * "period": "both",
     * "semester": "first",
     * "phase": 2,
     * "created_at": "29-03-2021 15:03:36",
     * "updated_at": "29-03-2021 15:03:36"
     * },
     * {
     * "id": 5,
     * "name": "Web & Mobile SS",
     * "code": "WS01-15",
     * "credit": 12,
     * "period": "both",
     * "semester": "first",
     * "phase": 2,
     * "created_at": "29-03-2021 15:03:36",
     * "updated_at": "29-03-2021 15:03:36"
     * },
     * {
     * "id": 6,
     * "name": "Project & wetenschappelijk Rapporteren",
     * "code": "PR01-15",
     * "credit": 6,
     * "period": "both",
     * "semester": "second",
     * "phase": 2,
     * "created_at": "29-03-2021 15:03:36",
     * "updated_at": "29-03-2021 15:03:36"
     * },
     * {
     * "id": 7,
     * "name": "Algorithms & Data",
     * "code": "AD09-15",
     * "credit": 6,
     * "period": "first",
     * "semester": "second",
     * "phase": 2,
     * "created_at": "29-03-2021 15:03:36",
     * "updated_at": "29-03-2021 15:03:36"
     * },
     * {
     * "id": 8,
     * "name": "Data Science",
     * "code": "DS09-15",
     * "credit": 6,
     * "period": "second",
     * "semester": "second",
     * "phase": 2,
     * "created_at": "29-03-2021 15:03:36",
     * "updated_at": "29-03-2021 15:03:36"
     * },
     * {
     * "id": 9,
     * "name": "Web & Mobile FS",
     * "code": "WF01-15",
     * "credit": 12,
     * "period": "both",
     * "semester": "second",
     * "phase": 2,
     * "created_at": "29-03-2021 15:03:36",
     * "updated_at": "29-03-2021 15:03:36"
     * }
     * ]
     * }
     * ]
     * }
     *
     */
    public function index()
    {
        return ['data' => Trajectory::with('subjects')
            ->where('user_id', Auth::user()->id)
            ->get()];
    }

    /**
     * Create a new trajectory
     *
     * Add a new trajectory with a collection of subjects to Database.
     *
     * @bodyParam favorite number(0,1) required
     * To add a trajectory in favorite. Example: 1
     *
     * @bodyParam shared number(0,1) required
     * To share a trajectory. Example: 1
     *
     * @bodyParam subjects array required
     * Add a list of subjects->id in trajectory. Example: [1,2,3]
     *
     * @response {
     *      "status": 201,
     *      "success": true,
     *      "message": "Trajectory has been created"
     * }
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'subjects' => 'required|exists:subjects,id',
        ]);
        $trajectory = new Trajectory($request->all());
        $trajectory->user_id = $request->user()->id;
        $trajectory->save();
        Trajectory::findorfail($trajectory->id)->subjects()->sync($request->subjects);
        return response()->json(['message' => ' Trajectory has been created'], 201);
    }

    /**
     * Get one trajectory.
     *
     * Returns a trajectory with a list of all its subjects.
     *
     * @response {"data": [
     * {
     * "id": 1,
     * "favorite": 0,
     * "accepted": 0,
     * "shared": 1,
     * "user_id": 2,
     * "created_at": "29-03-2021 15:03:36",
     * "updated_at": "29-03-2021 15:03:36",
     * "subjects": [
     * {
     * "id": 1,
     * "name": "Professionele Communicatie",
     * "code": "PC01-15",
     * "credit": 3,
     * "period": "both",
     * "semester": "first",
     * "phase": 2,
     * "created_at": "29-03-2021 15:03:36",
     * "updated_at": "29-03-2021 15:03:36"
     * },
     * {
     * "id": 2,
     * "name": "Ethiek",
     * "code": "ET01-15",
     * "credit": 3,
     * "period": "both",
     * "semester": "first",
     * "phase": 2,
     * "created_at": "29-03-2021 15:03:36",
     * "updated_at": "29-03-2021 15:03:36"
     * }
     * ]
     * }
     * ]
     * }
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return ['data' => Trajectory::with('subjects')
            ->where('user_id', Auth::user()->id)
            ->findOrFail($id)];
    }

    /**
     * Update an existing trajectory.
     *
     * Update the subjects inside an existing trajectory.
     *
     *  @pathParam id number required
     * The id of the trajectory. Example: 1
     *
     * @bodyParam subjects array required
     * Add a list of subjects->id in trajectory. Example: [1,2,3]
     *
     * @response {
     *      "status": 200,
     *      "success": true,
     *      "message": "Trajectory has been updated"
     * }
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate([
            'subjects' => 'required|exists:subjects,id',
        ]);
        Trajectory::find($id)->subjects()
            ->where('user_id', Auth::user()->id)
            ->sync($request->subjects);
        return response()->json(['message' => ' Trajectory has been updated'], 200);
    }

    /**
     * Remove a trajectory from database.
     *
     * @pathParam id number required
     * The id of the trajectory. Example: 1
     *
     * @response {
     *      "status": 204,
     *      "success": true,
     *      "message": null
     * }
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $trajectory = Trajectory::Where('user_id', Auth::user()->id)
            ->findorfail($id);
        $trajectory->delete();
        return response()->json(null, 204);
    }

    public function favorite(Trajectory $trajectory)
    {
        $trajectory1 = Trajectory::Where('user_id', Auth::user()->id)
            ->findOrFail($trajectory->id);

        if ($trajectory1->favorite === 0){
            $trajectory1->favorite = 1;
        }
        else {
            $trajectory1->favorite = 0;
        }
        $trajectory1->save();
        return response()->json(['message' => ' Trajectory has been updated'], 200);
    }

    public function share(Trajectory $trajectory)
    {
        $trajectory1 = Trajectory::Where('user_id', Auth::user()->id)
            ->findOrFail($trajectory->id);

        if ($trajectory1->shared === 0){
            $trajectory1->shared = 1;
        }
        else {
            $trajectory1->shared = 0;
        }
        $trajectory1->save();
        return response()->json(['message' => ' Trajectory has been updated'], 200);
    }
}
