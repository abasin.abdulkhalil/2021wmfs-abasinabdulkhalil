<?php

namespace App\Http\Controllers;

use App\Models\Trajectory;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TrajectoryController extends Controller
{
    //
    public function overview(){
        $trajectories [] = null;
        if (Auth::user()->role->role == 'admin'){
            $trajectories = Trajectory::where('shared', 1)
                ->where('accepted', 0)
                ->get();
        }else{
            $trajectories = Trajectory::where('user_id', Auth::user()->id)
                ->get();
        }

        return view('trajectories',['trajectories' => $trajectories]);
    }

    public function trajectoryShow(Trajectory $trajectory){
        $subjectsOfTrajectory = \App\Models\Subject::whereHas('trajectories', function (Builder $query) use ($trajectory){
                $query->where('id', $trajectory->id);
            })->get();
        return view('trajectories-show',['subjectsOfTrajectory' => $subjectsOfTrajectory, 'trajectory' => $trajectory]);
    }

    public function trajectoryShowAccept(Trajectory $trajectory){
        return view('trajectories-accept', ['trajectory' => $trajectory]);
    }
    public function trajectoryAccept(Trajectory $trajectory){
        $trajectory1 = Trajectory::findOrFail($trajectory->id);
        $trajectory1->accepted = 1;
        $trajectory1->save();

        return redirect('/dashboard/trajectories');
    }
    public function trajectoryShowDelete(Trajectory $trajectory){
        return view('trajectories-delete', ['trajectory' => $trajectory]);
    }

    public function trajectoryDelete(Trajectory $trajectory){
        $trajectory->delete();
        return redirect('/dashboard/trajectories');
    }
}
