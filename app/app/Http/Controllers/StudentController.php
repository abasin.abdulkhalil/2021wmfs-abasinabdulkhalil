<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class StudentController extends Controller
{

    //
    /**
     * StudentController constructor.
     */
    public function __construct()
    {

    }

    public function overview(){
        //Gate::authorize('isAdmin');
        $users = User::Where('role_id', '2')->get();
        return view('students', ['users' => $users]);
    }

    public function studentShowDelete(User $student){
        //Gate::authorize('isAdmin');
        return view('delete-model', ['model' => $student]);
    }

    public function studentDelete(User $student){
        //Gate::authorize('isAdmin');
        $student->delete();
        return redirect('/dashboard/students');
    }
}
