<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    use HasFactory;
    protected $hidden = ['pivot'];
    protected $casts = [
        'created_at' => 'datetime:d-m-Y H:m:s',
        'updated_at' => 'datetime:d-m-Y H:m:s',
    ];
    protected $fillable = ['id', 'name', 'code', 'credit', 'period', 'semester', 'phase'];

    public function getCodeAttribute($value){
        return strtoupper($value);
    }
    public function setCodeAttribute($value){
        $this->attributes['code'] = strtoupper($value);
    }

    public function trajectories(){
        return $this->belongsToMany(Trajectory::class);
    }
}
