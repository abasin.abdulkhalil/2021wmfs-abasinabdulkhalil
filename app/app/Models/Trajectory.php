<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Trajectory extends Model
{
    use HasFactory;

    protected $fillable = ['favorite', 'accepted', 'shared', 'user_id'];
    protected $casts = [
        'created_at' => 'datetime:d-m-Y H:m:s',
        'updated_at' => 'datetime:d-m-Y H:m:s',
    ];

    public function subjects(){
        return $this->belongsToMany(Subject::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }
}
