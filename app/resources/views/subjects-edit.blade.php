@extends('layouts/layout')

@section('title', 'dashboard')
@section('content')


<!-- Page Inner -->
<div class="page-inner">
    <div class="page-title">
        <h3 class="breadcrumb-header">Vak bewerken</h3>
    </div>
    <div id="main-wrapper">
        <div class="row">



            <div class="panel panel-white col-sm-6">
                <div class="panel-body">
                    <form action="{{ url('dashboard/subjects/'. $subject->id . '/edit') }}" method="post" enctype="multipart/form-data">
                        @csrf

                        <div class="form-group">
                            <label for="name">Naam</label>
                            <input type="text" name="name" id="name" class="form-control" placeholder="Naam" value="{{$subject->name}}">
                        </div>
                        <div class="form-group">
                            <label for="credit">Studiepunten</label>
                            <input type="number" name="credit" id="credit" class="form-control" placeholder="Studiepunten" value="{{$subject->credit}}">
                        </div>
                        <div class="form-group">
                            <label for="period">Periode</label>
                            <select style="margin-bottom:15px;" name="period" id="period" class="form-control">
                                <option value="first" @if($subject->period === 'first') selected="selected" @endif>Eerste periode</option>
                                <option value="second" @if($subject->period === 'second') selected="selected" @endif>Tweede periode</option>
                                <option value="both" @if($subject->period === 'both') selected="selected" @endif>Eerste en tweede periode</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="semester">Semester</label>
                            <select style="margin-bottom:15px;" name="semester" id="semester" class="form-control">
                                <option value="first" @if($subject->semester === 'first') selected="selected" @endif>Eerste semester</option>
                                <option value="second" @if($subject->semester === 'second') selected="selected" @endif>Tweede semester</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="phase">Phase</label>
                            <select style="margin-bottom:15px;" name="phase" id="phase" class="form-control">
                                <option value= "1" @if($subject->phase === 1) selected="selected" @endif>Eerste jaar</option>
                                <option value="2" @if($subject->phase === 2) selected="selected" @endif>Tweede jaar</option>
                                <option value="3" @if($subject->phase === 3) selected="selected" @endif>Derde jaar</option>
                            </select>
                        </div>

                        <input name="moduleAction" type="hidden" value="update"/>
                        <input name="id" type="hidden" value="{{$subject->id}}"/>


                        <button type="submit" class="btn btn-primary">Opslaan</button>
                    </form>
                </div>
            </div>

            @include('layouts.errors')




        </div><!-- Row -->
    </div><!-- Main Wrapper -->

@endsection
