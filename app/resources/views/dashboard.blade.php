@extends('layouts/layout')

@section('title', 'dashboard')
@section('content')


    <!-- Page Inner -->
    <div class="page-inner">
        <div class="page-title">
            <h3 class="breadcrumb-header">Beheerderspaneel</h3>
        </div>
        <div id="main-wrapper">
            <div class="row">


                @if(Auth::user()->role->role == 'admin')
                <div class="col-md-4">

                    <div class="panel panel-white">
                        <div class="panel-heading clearfix">
                            <h4 class="panel-title">Vakken toevoegen</h4>
                        </div>
                        <div class="panel-body">
                            <P>Om een vak toe te voegen klik op de knop onderaan.</P>
                            <a href="{{url('/dashboard/subjects/add')}}" class="btn btn-primary">Vak toevoegen</a>
                        </div>
                    </div>
                </div>
                @endif

                <div class="col-md-4">
                    <div class="panel panel-white">
                        <div class="panel-heading clearfix">
                            <h4 class="panel-title">Trajecten</h4>
                        </div>
                        <div class="panel-body">
                            <p>Klik op de knop hieronder om de trajecten te bekijken.</p>
                            <a href="{{url('/dashboard/trajectories')}}" class="btn btn-danger">Trajecten</a>

                        </div>
                    </div>

                </div>




            </div><!-- Row -->
        </div><!-- Main Wrapper -->

@endsection
