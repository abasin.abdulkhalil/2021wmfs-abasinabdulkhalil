<!DOCTYPE html>
<html lang="en" >
<head>
    <meta charset="UTF-8">
    <title>Login</title>

    <link href="{{asset('css/style.css')}}" rel="stylesheet">

</head>
<body>

<div class="mijnErrors">
    @include('layouts.errors')
</div>



<!-- partial:index.partial.html -->
<div class="wrapper">
    <div class="container">
        <h1>Welcome</h1>

        <form method="POST" action="{{ route('register') }}" class="form">
            @csrf
            <input type="text" placeholder="Name" name="name" id="name" value="{{old('name', '')}}">
            <input type="text" placeholder="Surname" name="surname" id="surname" value="{{old('surname', '')}}">
            <input type="text" placeholder="Email" name="email" id="email" value="{{old('email', '')}}">
            <input type="password" placeholder="Password" name="password" id="password">
            <input type="password" placeholder="Confirm Password" name="password_confirmation" id="password_confirmation">
            <button type="submit">Login</button>
        </form>
    </div>
    <ul class="bg-bubbles">
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
    </ul>
</div>
<!-- partial -->
<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script><script  src="{{asset('js/script.js')}}"></script>

</body>
</html>

