@extends('layouts/layout')

@section('title', 'dashboard')
@section('content')


<!-- Page Inner -->
<div class="page-inner">
    <div class="page-title">
        <h3 class="breadcrumb-header">Vak toevoegen</h3>
    </div>
    <div id="main-wrapper">
        <div class="row">



            <div class="panel panel-white col-sm-6">
                <div class="panel-body">
                    <form action="{{ url('dashboard/subjects/add') }}" method="post" enctype="multipart/form-data">
                        @csrf

                        <div class="form-group">
                            <label for="name">Naam</label>
                            <input type="text" name="name" id="name" class="form-control" placeholder="Naam" value="{{old('name', '')}}">
                        </div>
                        <div class="form-group">
                            <label for="code">Code</label>
                            <input type="text" name="code" id="code" class="form-control" placeholder="Code" value="{{old('code', '')}}">
                        </div>
                        <div class="form-group">
                            <label for="credit">Studiepunten</label>
                            <input type="number" name="credit" id="credit" class="form-control" placeholder="Studiepunten" value="{{old('credit', 0)}}">
                        </div>
                        <div class="form-group">
                            <label for="period">Periode</label>
                            <select style="margin-bottom:15px;" name="period" id="period" class="form-control">
                                <option value="first" @if(old('period', '') === 'first') selected="selected" @endif>Eerste periode</option>
                                <option value="second" @if(old('period', '') === 'second') selected="selected" @endif>Tweede periode</option>
                                <option value="both" @if(old('period', '') === 'both') selected="selected" @endif>Eerste en tweede periode</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="semester">Semester</label>
                            <select style="margin-bottom:15px;" name="semester" id="semester" class="form-control">
                                <option value="first" @if(old('semester', '') === 'first') selected="selected" @endif>Eerste semester</option>
                                <option value="second" @if(old('semester', '') === 'second') selected="selected" @endif>Tweede semester</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="phase">Phase</label>
                            <select style="margin-bottom:15px;" name="phase" id="phase" class="form-control">
                                <option value= "1" @if(old('phase', '') === '1') selected="selected" @endif>Eerste jaar</option>
                                <option value="2" @if(old('phase', '') === '2') selected="selected" @endif>Tweede jaar</option>
                                <option value="3" @if(old('phase', '') === '3') selected="selected" @endif>Derde jaar</option>
                            </select>
                        </div>

                        <input name="moduleAction" type="hidden" value="create"/>

                        <button type="submit" class="btn btn-primary">Toevoegen</button>
                    </form>
                </div>
            </div>

            @include('layouts.errors')




        </div><!-- Row -->
    </div><!-- Main Wrapper -->

@endsection
