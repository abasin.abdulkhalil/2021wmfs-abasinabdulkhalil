@extends('layouts/layout')

@section('title', 'Trajectories')
@section('content')

    <!-- Page Inner -->
    <div class="page-inner">
        <div class="page-title">
            <h3 class="breadcrumb-header">Traject verwijderen</h3>
        </div>
        <div id="main-wrapper">
            <div class="row">



                <h3>Weet je zeker dat je deze traject wilt verwijderen?</h3>
                <form method="post" action="{{url('/dashboard/trajectories/'. $trajectory->id .'/delete')}}">
                    @csrf
                    <button type="submit" class="btn btn-danger">Verwijderen</button>
                </form>



            </div><!-- Row -->
        </div><!-- Main Wrapper -->

@endsection

