@extends('layouts/layout')

@section('title', 'Students')
@section('content')

    <!-- Page Inner -->
    <div class="page-inner">
        <div class="page-title">
            <h3 class="breadcrumb-header">Beheerderspaneel</h3>
        </div>
        <div id="main-wrapper">
            <div class="row">



                <div class="panel panel-white" id="js-alerts">
                    <div class="panel-heading clearfix">
                        <h4 class="panel-title">Studenten</h4>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Voornaam</th>
                                    <th>Achternaam</th>
                                    <th>Email</th>
                                    <th>Actie</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($users as $student)
                                <tr>
                                    <th scope="row">{{$student->id}}</th>
                                    <td>{{$student->name}}</td>
                                    <td>{{$student->surname}}</td>
                                    <td>{{$student->email}}</td>
                                    <td><a href="{{url('dashboard/students/' . $student->id) . '/delete'}}" class="btn btn-danger"><i class="fa fa-trash"></i></a></td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>



            </div><!-- Row -->
        </div><!-- Main Wrapper -->

@endsection

