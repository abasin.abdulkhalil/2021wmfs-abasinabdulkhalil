@extends('layouts/layout')

@section('title', 'Subjects info')
@section('content')

    <!-- Page Inner -->
    <div class="page-inner">
        <div class="page-title">
            <h3 class="breadcrumb-header">Weet je zeker dat je <i class="text-primary">{{$model->name}} {{$model->surname}}</i>  wilt verwijderen?</h3>
        </div>
        <div id="main-wrapper">
            <div class="row">

                @if(request()->is('dashboard/students*'))
                    <form method="post" action="{{url('/dashboard/students/' . $model->id). '/delete'}}">
                        @csrf
                        <button type="submit" class="btn btn-danger">Verwijder</button>
                    </form>
                @else
                    <form method="post" action="{{url('/dashboard/subjects/' . $model->id). '/delete'}}">
                        @csrf
                        <button type="submit" class="btn btn-danger">Verwijder</button>
                    </form>
                @endif

            </div><!-- Row -->
        </div><!-- Main Wrapper -->

@endsection
