@if($errors->any())
    <div class="col-sm-5">
        <div class="panel-body">
            <div class="alert alert-danger">
                <strong>Somthing went wrong!</strong>
                <br><br>
                <ul>
                    @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
@endif
