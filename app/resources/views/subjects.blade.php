@extends('layouts/layout')

@section('title', 'Subjects')
@section('content')

    <!-- Page Inner -->
    <div class="page-inner">
        <div class="page-title">
            <h3 class="breadcrumb-header">Beheerderspaneel</h3>
        </div>
        <div id="main-wrapper">
            <div class="row">

                <div class="panel panel-white">
                    <div class="panel-body">
                        <div class="panel-heading clearfix">
                            <h4 class="panel-title">Zoeken</h4>
                        </div>
                        <form action="{{ url('/dashboard/subjects') }}" method="get" enctype="multipart/form-data" class="form-inline">
                            @csrf

                            <div class="form-group m-r-lg">
                                <label for="name" class="m-r-md ">Naam</label>
                                <input type="text" name="name" id="name" class="form-control" placeholder="Naam" value="{{ Request::get('name') }}">
                            </div>
                            <div class="form-group m-r-lg">
                                <label for="semester" class="m-r-md">Semester</label>
                                <select name="semester" id="semester" class="form-control">
                                    <option value="" >Selecteren</option>
                                    <option value="first" @if(Request::get('semester') === 'first') selected="selected" @endif>Eerste semester</option>
                                    <option value="second" @if(Request::get('semester') === 'second') selected="selected" @endif>Tweede semester</option>
                                </select>
                            </div>
                            <div class="form-group m-r-lg p-v-xs">
                                <label for="phase" class="m-r-md">Phase</label>
                                <select name="phase" id="phase" class="form-control">
                                    <option value="" >Selecteren</option>
                                    <option value= "1" @if(Request::get('phase') === '1') selected="selected" @endif>Eerste jaar</option>
                                    <option value="2" @if(Request::get('phase') === '2') selected="selected" @endif>Tweede jaar</option>
                                    <option value="3" @if(Request::get('phase') === '3') selected="selected" @endif>Derde jaar</option>
                                </select>
                            </div>

                            <input name="moduleAction" type="hidden" value="create"/>

                            <input class="btn btn-primary" type="submit" value="search"/>

                        </form>
                    </div>
                </div>



                <div class="panel panel-white" id="js-alerts">
                    @if(count($subjects) > 0)
                    <div class="panel-heading clearfix">
                        <h4 class="panel-title">Vakken</h4>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th>Naam</th>
                                    <th>Code</th>
                                    <th>Studiepunten</th>
                                    <th>Periode</th>
                                    <th>Semester</th>
                                    <th>Fase</th>
                                    <th>Acties</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($subjects as $subject)
                                <tr>
                                    <th scope="row">{{$subject->name}}</th>
                                    <td>{{$subject->code}}</td>
                                    <td>{{$subject->credit}}</td>
                                    <td>{{$subject->period}}</td>
                                    <td>{{$subject->semester}}</td>
                                    <td>{{$subject->phase}}</td>
                                    <td>
                                        <a href="{{url('dashboard/subjects/' . $subject->id . '/info')}}" class="btn btn-success"><i class="fa fa-eye"></i></a>
                                        @if(Auth::user()->role->role == 'admin')
                                        <a href="{{url('dashboard/subjects/' . $subject->id . '/edit')}}" class="btn btn-warning"><i class="fa fa-edit"></i></a>
                                        <a href="{{url('dashboard/subjects/' . $subject->id . '/delete')}}" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                                        @endif
                                    </td>

                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    @else
                        <p>Geen Vakken gevonden</p>
                    @endif
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="text-center">
                            {{$subjects->links()}}
                        </div>
                    </div>
                </div>


            </div><!-- Row -->
        </div><!-- Main Wrapper -->

@endsection

