@extends('layouts/layout')

@section('title', 'Subjects info')
@section('content')

    <!-- Page Inner -->
    <div class="page-inner">
        <div class="page-title">
            <h3 class="breadcrumb-header">{{$subject->name}}</h3>
        </div>
        <div id="main-wrapper">
            <div class="row">



                <div class="col-md-12 col-sm-12  ">
                    <div class="x_panel">
                        <div class="x_content">
                            <dl>
                                <dt class="my-dt">Naam</dt>
                                <dd>{{$subject->name}}</dd>

                                <dt class="my-dt">Code</dt>
                                <dd>{{$subject->code}}</dd>

                                <dt class="my-dt">Aantal studiepunten</dt>
                                <dd>{{$subject->credit}}</dd>

                                <dt class="my-dt">Periode</dt>
                                @if($subject->period === 'first')
                                    <dd>Eerste periode</dd>
                                @elseif($subject->period === 'second')
                                    <dd>Tweede periode</dd>
                                @else
                                    <dd>Eerste en tweede periode</dd>
                                @endif

                                <dt class="my-dt">Semester</dt>
                                @if($subject->semester === 'first')
                                    <dd>Eerste semester</dd>
                                @elseif($subject->semester === 'second')
                                    <dd>Tweede semester</dd>
                                @else
                                    <dd>Eerste en tweede semester</dd>
                                @endif

                                <dt class="my-dt">Phase</dt>
                                @if($subject->phase === 1)
                                    <dd>Eerste jaar</dd>
                                @elseif($subject->phase === 2)
                                    <dd>Tweede jaar</dd>
                                @else
                                    <dd>Derde jaar</dd>
                                @endif
                            </dl>
                        </div>
                    </div>
                </div>



            </div><!-- Row -->
        </div><!-- Main Wrapper -->

@endsection

