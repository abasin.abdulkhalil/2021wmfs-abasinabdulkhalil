@extends('layouts/layout')

@section('title', 'Trajectories')
@section('content')

    <!-- Page Inner -->
    <div class="page-inner">
        <div class="page-title">
            <h3 class="breadcrumb-header">Gedeelde trajecten</h3>
        </div>
        <div id="main-wrapper">
            <div class="row">



                <div class="panel panel-white" id="js-alerts">
                    <div class="panel-heading clearfix">
                        <h4 class="panel-title">{{$trajectory->user->name}} {{$trajectory->user->surname}}</h4>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th>Naam vak</th>
                                    <th>Code</th>
                                    <th>Antal studiepunten</th>
                                    <th>Semester</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($subjectsOfTrajectory as $subject)
                                    <tr>
                                        <th scope="row"><i class="fa fa-book success"></i>	&nbsp;&nbsp;&nbsp;{{$subject->name}}</th>
                                        <td>{{$subject->code}}</td>
                                        <td>{{$subject->credit}}stp</td>
                                        @if($subject->semester === 'first')
                                            <td>Eerste semester</td>
                                        @elseif($subject->semester === 'second')
                                            <td>Tweede semester</td>
                                        @else
                                            <td>Eerste en tweede semester</td>
                                        @endif
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                @if(Auth::user()->role->role == 'admin')
                    <a class="my-btn btn btn-success" href="{{url('/dashboard/trajectories/' . $trajectory->id . '/accept')}}">Accepteren</a>
                @endif
                <a class="my-btn btn btn-danger" href="{{url('/dashboard/trajectories/' . $trajectory->id . '/delete')}}">Verwijderen</a>



            </div><!-- Row -->
        </div><!-- Main Wrapper -->

@endsection

