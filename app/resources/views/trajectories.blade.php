@extends('layouts/layout')

@section('title', 'Trajectories')
@section('content')

    <!-- Page Inner -->
    <div class="page-inner">
        <div class="page-title">
            <h3 class="breadcrumb-header">Gedeelde trajecten</h3>
        </div>
        <div id="main-wrapper">
            <div class="row">



                <div class="panel panel-white col col-sm-6" id="js-alerts">
                    <div class="panel-heading clearfix">
                        <h4 class="panel-title">Trajecten</h4>
                    </div>
                    <div class="panel-body">
                        <table class="table table-hover">
                            <tbody>
                            @foreach($trajectories as $key => $trajectory)
                            <tr>
                                @if(Auth::user()->role->role == 'admin')
                                    <td>{{$trajectory->user->name}} {{$trajectory->user->surname}} heeft een traject gedeeld</td>
                                @else
                                    <td>Traject {{$key +1}} &nbsp;&nbsp;&nbsp; van &nbsp;&nbsp;&nbsp; {{$trajectory->user->name}} {{$trajectory->user->surname}}</td>
                                @endif
                                <td><a href="{{url('/dashboard/trajectories/' . $trajectory->id)}}" class="btn btn-primary">Bekijken</a></td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>



            </div><!-- Row -->
        </div><!-- Main Wrapper -->

@endsection

