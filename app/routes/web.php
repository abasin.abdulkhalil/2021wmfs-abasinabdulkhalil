<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\StudentController;

use App\Http\Controllers\TrajectoryController;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\SubjectController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return redirect('/dashboard');
});
Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

//must be logged in to access these routes.
Route::middleware(['auth'])->group(function () {
    Route::get('/dashboard/subjects', [SubjectController::class, 'overview']);
    Route::get('/dashboard/subjects/{subject}/info', [SubjectController::class, 'subjectInfo']);
    Route::get('/dashboard/subjects/add', [SubjectController::class, 'subjectShowAdd'])->middleware('can:isAdmin');
    Route::post('/dashboard/subjects/add', [SubjectController::class, 'subjectAdd'])->middleware('can:isAdmin');
    Route::get('/dashboard/subjects/{subject}/edit', [SubjectController::class, 'subjectShowEdit'])->middleware('can:isAdmin');
    Route::post('/dashboard/subjects/{subject}/edit', [SubjectController::class, 'subjectEdit'])->middleware('can:isAdmin');
    Route::get('/dashboard/subjects/{subject}/delete', [SubjectController::class, 'subjectShowDelete'])->middleware('can:isAdmin');
    Route::post('/dashboard/subjects/{subject}/delete', [SubjectController::class, 'subjectDelete'])->middleware('can:isAdmin');

    Route::get('/dashboard/students', [StudentController::class, 'overview'])->middleware('can:isAdmin');
    Route::get('/dashboard/students/{student}/delete', [StudentController::class, 'studentShowDelete'])->middleware('can:isAdmin');
    Route::post('/dashboard/students/{student}/delete', [StudentController::class, 'studentDelete'])->middleware('can:isAdmin');

    Route::get('/dashboard/trajectories', [TrajectoryController::class, 'overview']);
    Route::get('/dashboard/trajectories/{trajectory}', [TrajectoryController::class, 'trajectoryShow'])->middleware('can:view,trajectory');
    Route::get('/dashboard/trajectories/{trajectory}/accept', [TrajectoryController::class, 'trajectoryShowAccept'])->middleware('can:isAdmin');
    Route::post('/dashboard/trajectories/{trajectory}/accept', [TrajectoryController::class, 'trajectoryAccept'])->middleware('can:isAdmin');
    Route::get('/dashboard/trajectories/{trajectory}/delete', [TrajectoryController::class, 'trajectoryShowDelete'])->middleware('can:delete,App\Models\Trajectory,trajectory');
    Route::post('/dashboard/trajectories/{trajectory}/delete', [TrajectoryController::class, 'trajectoryDelete'])->middleware('can:delete,trajectory');
});

Route::post('/sanctum/login', function (Request $request) {
    $credentials = $request->only('email', 'password');
    if (Auth::attempt($credentials)) {
        return $request->user();
    }
    return response(['message' => 'The provided credentials do not match our records.'], 422);
});

Route::post('/sanctum/logout', function (Request $request) {
    Auth::guard('web')->logout();
    $request->session()->invalidate();
    return response(['message' => 'The user has been logged out successfully'], 200);
});




require __DIR__.'/auth.php';
