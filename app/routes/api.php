<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::apiResource('subjects', \App\Http\Controllers\SubjectApiController::class);
Route::apiResource('trajectories', \App\Http\Controllers\TrajectoryApiController::class)->middleware('auth:sanctum');
Route::post('trajectories/{trajectory}/favorite', [\App\Http\Controllers\TrajectoryApiController::class, 'favorite'])->middleware('auth:sanctum');
Route::post('trajectories/{trajectory}/share', [\App\Http\Controllers\TrajectoryApiController::class, 'share'])->middleware('auth:sanctum');

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::apiResource('/register', \App\Http\Controllers\Auth\RegisteredUserController::class);
