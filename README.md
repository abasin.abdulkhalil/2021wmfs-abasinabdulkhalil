# 2021WMFS-AbasinAbdulkhalil

Teaser Trajectplanner

## Installation instructions

* Install [git](https://git-scm.com/downloads) and [Docker Desktop](https://www.docker.com/products/docker-desktop).
* Start the Docker Desktop application
* Run from your terminal/cmd

```shell
git clone https://git.ikdoeict.be/abasin.abdulkhalil/2021wmfs-abasinabdulkhalil.git
````
* When Docker is up and running, run from your terminal/cmd

```shell
cd 2021wmfs-abasinabdulkhalil
docker-compose up
````

* When the containers are up and running, run from a new terminal/cmd

```shell
cd 2021wmfs-abasinabdulkhalil
docker-compose exec php-web bash
```

* From the Bash terminal in the php-web container, run the following commands:

```shell
composer install
cp .env.example .env
php artisan key:generate
php artisan migrate
php artisan db:seed --class=trajectPlanner
````

* Browse to http://localhost:8085 for the server side
* Browse to http://localhost:4000 for the client side

    - <i>If you are getting any error, run from a new terminal/cmd</i>
```shell
cd 2021wmfs-abasinabdulkhalil
docker-compose down
docker-compose up --build
```
* Everything should work fine now
* Stop the environment in your terminal/cmd by pressing `Ctrl+C`
* In order to avoid conflicts with other docker environment, run from your terminal/cmd

```shell
docker-compose down
```


## url design

* http://localhost:4000
* http://localhost:4000/nieuwtraject
* http://localhost:4000/trajecten
* http://localhost:4000/trajecten/favoriet
* http://localhost:4000/trajecten/gedeeld
* http://localhost:4000/trajecten/geaccepteerd
  
* http://localhost:4000/inloggen
* http://localhost:4000/registreren

